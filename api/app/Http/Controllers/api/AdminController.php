<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\InspectionSlot;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function index(){
        return response()->api(true,"successful",InspectionSlot::where("appointment",">=",date("Y-m-d H:i:s"))->orderBy('appointment', 'asc')->get());
    }
}
