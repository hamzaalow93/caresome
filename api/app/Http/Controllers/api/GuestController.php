<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\InspectionRequest;
use App\InspectionSlot;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    public function free_times($date)
    {
        $format = "Y-m-d";
        //check date format
        if (date($format, strtotime($date)) != date($date)) {
            return response()->api(false, "Date format is not suitable");
        }
        //check date after today
        if (strtotime($date) < strtotime(date($format))) {
            return response()->api(false, "Date should be after or equal today");
        }
        //check date less than three weeks
        if (strtotime($date) > strtotime("+3 weeks")) {
            return response()->api(false, "Date should be less than three weeks from now");
        }
        $appointments = InspectionSlot::getFreeAppointments($date);
        if(count($appointments)>0) return response()->api(true, 'successful', $appointments);
        return response()->api(false, 'No appointments for the selected date.. please choose another date');

    }
    public function appoint(InspectionRequest $request){
        $inspection=new InspectionSlot();
        $inspection->appointment=$request->appointment;
        $inspection->email=$request->email;
        $inspection->mobile=$request->mobile;
        $inspection->save();
        return response()->api(true, 'successful', $inspection,201);
    }
}
