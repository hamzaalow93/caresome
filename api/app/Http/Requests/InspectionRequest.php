<?php

namespace App\Http\Requests;

use App\Rules\AppointmentRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class InspectionRequest extends FormRequest
{
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(
            response()->api(false, 'Validation error', $validator->errors()->all(), 422)
        );
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'appointment'=>[
                "required",
                "date_format:Y-m-d H:i:s",
                'before:+3 weeks',
                new AppointmentRule
            ],
            'email'=>'required|email',
            'mobile'=>'required|numeric'

        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'appointment' => $this->date.' '.$this->time,
        ]);
    }
}
