<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InspectionSlot extends Model
{
    public static function getFreeAppointments($date)
    {
        $dayName = date('l', strtotime($date));
        $dayRole = WeekDaysRole::where('name', $dayName)->first();
        if ($dayRole->slot_count == 0) return [];
        $appointments = [];
        $start = strtotime($dayRole->start);
        while ($start < strtotime($dayRole->end)) {
            if (
                InspectionSlot::where('appointment', $date . ' ' . date("H:i:s", $start))->count() < $dayRole->slot_count) {
                if ($date==date("Y-m-d")){
                    if($start > strtotime('+60 minutes', strtotime(date('H:i:s')))) {
                        array_push($appointments, date("H:i:s", $start));
                    }
                }else{
                    array_push($appointments, date("H:i:s", $start));
                }
            }
            $start = strtotime('+30 minutes', $start);
        }
        return $appointments;
    }

    public static function checkAvailableAppointment($date)
    {
        $dayName = date('l', strtotime($date));
        $dayRole = WeekDaysRole::where('name', $dayName)->first();
//        chosen Day is Off day
        if ($dayRole->slot_count == 0) return false;
//        Chosen appointment is less than one hour from now
        if (strtotime($date) <= strtotime('+60 minutes', strtotime(date('H:i:s')))) return false;
//        Chosen appointment is not available
        if (InspectionSlot::
            where('appointment', $date)
                ->count() >= $dayRole->slot_count) return false;
        return true;
    }
}
