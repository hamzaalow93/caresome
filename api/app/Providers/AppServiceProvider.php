<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('api', function ($status=true,$message="Successful",$data=null,$code=200) {
            return Response::make([
                'status'=>$status,
                'message'=>$message,
                'data'=>$data
            ],$code);
        });
        Schema::defaultStringLength(191);
    }
}
