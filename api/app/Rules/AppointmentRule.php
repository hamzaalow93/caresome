<?php

namespace App\Rules;

use App\InspectionSlot;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Validation\Validator;

class AppointmentRule implements Rule
{

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return InspectionSlot::checkAvailableAppointment($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Appointment is not available,please choose another appointment';
    }
}
