<?php

use Illuminate\Database\Seeder;
use App\User;
use App\WeekDaysRole;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        default user
        $user = new User();
        $user->password = Hash::make('102030405060');
        $user->email = 'hamza@carsome.com';
        $user->name = 'Hamza Alow';
        $user->save();
//        default week days
        $days = [
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday'
        ];
        foreach ($days as $day) {
            $role = new WeekDaysRole();
            $role->name = $day;
            $role->start = "09:00:00";
            $role->end = "18:00:00";
            switch ($day) {
                case "Sunday":
                    $role->slot_count = 0;
                    break;
                case "Saturday":
                    $role->slot_count = 4;
                    break;
                default:
                    $role->slot_count = 2;
                    break;
            }
            $role->save();
        }

    }
}
