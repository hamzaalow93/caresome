<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
Route::group([
    "namespace"=>"api"
], function ($router) {
    Route::get('login', function (){
        return response()->api(false,'Unauthorized - please login');
    })->name('login');
    Route::get('Free-Times/{date}','GuestController@free_times');
    Route::post("Appoint",'GuestController@appoint');
});


Route::group([
    'middleware' => 'api',
    "namespace"=>"api"
], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::get("Inspections","AdminController@index");
});

