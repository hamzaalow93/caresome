import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { InspectionSlotComponent } from './inspection-slot/inspection-slot.component';
import { BookingComponent } from './booking/booking.component';
import { CanActivateAdmin } from './common/CanActivateAdmin';
import { CanActivateGuest } from './common/CanActivateGuest';

const routes: Routes = [
  {
    path:"",
    component:HomeComponent
  },
  {
    path:"inspection-slots",
    component:InspectionSlotComponent,
    canActivate:[CanActivateAdmin]
  },
  {
    path:"login",
    component:LoginComponent,
    canActivate:[CanActivateGuest]
  },
  {
    path:"booking",
    component:BookingComponent,
    canActivate:[CanActivateGuest]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
