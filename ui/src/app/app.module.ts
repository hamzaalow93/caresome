import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { BookingComponent } from './booking/booking.component';
import { LoginComponent } from './login/login.component';
import { InspectionSlotComponent } from './inspection-slot/inspection-slot.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common'
import {
  MatInputModule,
  MatFormFieldModule,
  MatStepperModule,
  MatButtonModule,
  MatChipsModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatSnackBarModule
} from '@angular/material';
import { InspectionSlotService } from './services/inspection-slot.service';
import { AuthService } from './services/auth.service';
import { CanActivateAdmin } from './common/CanActivateAdmin';
import { CanActivateGuest } from './common/CanActivateGuest';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BookingComponent,
    LoginComponent,
    InspectionSlotComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatStepperModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatChipsModule,
    MatSnackBarModule
  ],
  providers: [
    DatePipe,
    InspectionSlotService,
    AuthService,
    CanActivateAdmin,
    CanActivateGuest
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
