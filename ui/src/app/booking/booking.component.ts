import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { InspectionSlotService } from '../services/inspection-slot.service';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  appointments;
  appointmentSelected;
  matcher = new MyErrorStateMatcher();
  message="Loading.....";
  firstFormGroup= new FormGroup({
    email: new FormControl(
      '',
      [
        Validators.required,
        Validators.email
      ]
    ),
    mobile: new FormControl(
      '',
      [
        Validators.required,
        Validators.pattern(/^-?(0|[0-9]\d*)?$/)
      ]
      ),
  });
  secondFormGroup = new FormGroup({
    appointmentDate: new FormControl(new Date(), [Validators.required]),
  });
  minDate: Date;
  maxDate: Date=new Date();
  constructor(private http:InspectionSlotService,private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.appointments=[];
    this.minDate = new Date();
    this.maxDate.setDate((new Date() ).getDate() + 21);
  }
  get email(){
    return this.firstFormGroup.get('email');
  }
  get mobile(){
    return this.firstFormGroup.get('mobile');
  }
  get appointmentDate(){
    return this.secondFormGroup.get('appointmentDate');
  }
  getFreeTimes(stepper){
    this.appointments=[];
    this.http.getFreeTime(this.appointmentDate).subscribe((result:any)=> {
      if(result.status && result.data.length){
        this.appointments=result.data;
        this.appointmentSelected=this.appointments[0];
        stepper.next();
      }else{
        this.message=result.message;
        this._snackBar.open(result.message, 'End now', {
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    });
  }
  select(appointment){
    this.appointmentSelected=appointment;
  }
  addAppointment(stepper){
    this.http.addAppointment({
      date:this.appointmentDate,
      time:this.appointmentSelected,
      email:this.email.value,
      mobile:this.mobile.value
    }).subscribe((result:any)=>{
      if(result.status){
        this._snackBar.open(result.message, 'End now', {
          duration: 1500,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
        stepper.reset();
      }else{
        this._snackBar.open(result.message, 'End now', {
          duration: 700,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    });
  }
}
