import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { CanActivate } from '@angular/router/src/utils/preactivation';

@Injectable()
export class CanActivateAdmin implements CanActivate {
  constructor(private auth:AuthService,private router:Router) {}
    path: ActivatedRouteSnapshot[];
    route: ActivatedRouteSnapshot;

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean|UrlTree>|Promise<boolean|UrlTree>|boolean|UrlTree {
    if(this.auth.isLoging()) return true;
    this.router.navigate(["/login"]);
  }
}