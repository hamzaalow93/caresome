import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionSlotComponent } from './inspection-slot.component';

describe('InspectionSlotComponent', () => {
  let component: InspectionSlotComponent;
  let fixture: ComponentFixture<InspectionSlotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectionSlotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionSlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
