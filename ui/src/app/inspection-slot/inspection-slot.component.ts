import { Component, OnInit } from '@angular/core';
import { InspectionSlotService } from '../services/inspection-slot.service';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-inspection-slot',
  templateUrl: './inspection-slot.component.html',
  styleUrls: ['./inspection-slot.component.css']
})
export class InspectionSlotComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  inspections=[];
  constructor(private http:InspectionSlotService,private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.http.all().subscribe((result:any)=>{
      if(result.status){
        this.inspections=result.data;
      }else{
        this._snackBar.open(result.message, 'End now', {
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    });
  }

}
