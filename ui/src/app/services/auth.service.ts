import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { BASE_URL } from './config';
import { catchError } from 'rxjs/operators';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  constructor(private http: HttpClient, private _snackBar: MatSnackBar) {

  }
  login(credentials){
    return this.http.post(BASE_URL+"login",credentials);
  }
  isLoging(){
    if(localStorage.getItem('token')!=null) return true;
    return false;
  }
  logout(){
    localStorage.removeItem('token')
  }
}
