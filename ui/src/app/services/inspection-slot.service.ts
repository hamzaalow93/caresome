import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { DatePipe } from '@angular/common'
import { BASE_URL } from './config';

@Injectable({
  providedIn: 'root'
})
export class InspectionSlotService {
  constructor(private http: HttpClient,public datepipe: DatePipe) { }

  all(){
    let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', "Bearer "+localStorage.getItem("token"));
    console.log(headers);
    
    return this.http.get(BASE_URL+"Inspections",{ headers: headers });
  }
  getFreeTime(appointmentDate){
    let appointment_date =this.datepipe.transform(appointmentDate.value, 'yyyy-MM-dd');
    return this.http.get(BASE_URL+"Free-Times/"+appointment_date);
  }
  addAppointment(data){
    let formatedDate=this.datepipe.transform(data.date.value, 'yyyy-MM-dd');
    return this.http.post(BASE_URL+"Appoint",{
      date:formatedDate,
      time:data.time,
      email:data.email,
      mobile:data.mobile
    });
  }

}
